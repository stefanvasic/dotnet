﻿using MVCMessage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using MVCMessage.ViewModels;

namespace MVCMessage.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        [AllowAnonymous]
        public ActionResult Index()
        {
            HomeViewModel model = new HomeViewModel();
            var currentUser = db.Users.Find(User.Identity.GetUserId());
            if(currentUser!= null)
            {
               model.LastLoggedIn = currentUser.LastLoggedIn;
               model.NrOfLoggins = currentUser.NrOfLoggins;
               model.UnReadMessages = 0;
               model.UserName = currentUser.UserName;
                var query = from b in db.messages where b.ReceiverUser.Id.Equals(currentUser.Id) where b.IsRead==false
                           select new { b.MessageId };
                int count = 0;

                foreach(var item in query)
                {
                    count++;
                }
                model.UnReadMessages = count;

            }
           
            
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
             

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}