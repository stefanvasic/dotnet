﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCMessage.Models;
using Microsoft.AspNet.Identity;
using MVCMessage.ViewModels;

namespace MVCMessage.Controllers
{
    [Authorize]
    public class MessagesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Messages
        public ActionResult Index()
        {
            var currentUser = db.Users.Find(User.Identity.GetUserId());
            return View(db.messages.ToList().Where(message => message.ReceiverUser.Id == currentUser.Id));
        }

        // GET: Messages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        // GET: Messages/Create
        public ActionResult Create()
        {
            var model = new CreateMessageViewModel();
            var list = new List<SelectListItem>();
            var query = from b in db.Users select new { b.Email, b.UserName };
            foreach ( var item in query)
            {
                list.Add(new SelectListItem
               {
                    Value = item.UserName,
                    Text = item.Email
                });
            }

            model.ReceiverList = new SelectList(list, "Text", "Value");
            return View(model);
        }

        // POST: Messages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateMessageViewModel model)
        {
            var modelNew = new CreateMessageViewModel();
            Message message = new Message();
            message.Receiver = model.Receiver;
            message.Headline = model.Headline;
            message.Text = model.Text;
            var receiver = model.Receiver;
            var currentUser = db.Users.Find(User.Identity.GetUserId());
            var qry = from a in db.Users where a.Email.Equals(receiver) select a;
            ApplicationUser user = null;
            foreach (var item in qry)
            {
                user = item;

            }

            if (ModelState.IsValid && user != null)
            {
                message.ReceiverUser = user;
                message.SenderUser = currentUser;
                message.Date = DateTime.Now;
                currentUser.SentMessagesTotal = currentUser.SentMessagesTotal + 1;
                db.Entry(currentUser);
                db.messages.Add(message);
                db.SaveChanges();
                modelNew.Message = "Meddelande nummer " + currentUser.SentMessagesTotal +  " avsänt till " + message.ReceiverUser.UserName + ", " + message.Date;
                ModelState.Clear();
            }
            var list = new List<SelectListItem>();

            var query = from b in db.Users select new { b.Email, b.UserName };
            foreach (var item in query)
            {
                list.Add(new SelectListItem
                {
                    Value = item.UserName,
                    Text = item.Email
                });
            }

            
            modelNew.ReceiverList = new SelectList(list, "Text", "Value");
            return View(modelNew);
        }

        // GET: Messages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        // POST: Messages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MessageId,Receiver,Headline,Text,IsRead")] Message message)
        {
            if (ModelState.IsValid)
            {
                db.Entry(message).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(message);
        }

        // GET: Messages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        // POST: Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Message message = db.messages.Find(id);
            db.messages.Remove(message);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
