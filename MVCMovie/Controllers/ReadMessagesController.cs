﻿using Microsoft.AspNet.Identity;
using MVCMessage.Models;
using MVCMessage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MVCMessage.Controllers
{
    [Authorize]
    public class ReadMessagesController : Controller
    {
        // GET: ReadMessages
        public ActionResult Index()
        {
            var db = new ApplicationDbContext();
            var currentUser = db.Users.Find(User.Identity.GetUserId());
            var model = new ReadMessageViewModel();
            model.usernames = new List<string>();
            var query = from b in db.messages where b.ReceiverUser.Id.Equals(currentUser.Id) select b.SenderUser.UserName;
            
            foreach(var item in query)
            {
                if (!model.usernames.Contains(item))
                {
                    model.usernames.Add(item);
                }
            }

            model.deletedMessages = currentUser.DeletedMessages;
            var totalMessages = 0;
            var totalReadMessages = 0;
            var query2 = from b in db.messages
                        where b.ReceiverUser.Id.Equals(currentUser.Id)
                        select new { b.MessageId, b.IsRead };

            foreach (var item in query2)
            {
                totalMessages++;
                if (item.IsRead)
                {
                    totalReadMessages++;
                }
            }
            model.totalMessages = totalMessages;
            model.totalReadMessages = totalReadMessages;

            return View(model);
        }

        public ActionResult ReadMessagesFromUser(string username, int? id)
        {
            var db = new ApplicationDbContext();
            if (id != null)
            {
                int x = id.GetValueOrDefault();
                var deleteMessage = from b in db.messages where b.MessageId.Equals(x) select b;
                foreach (var item in deleteMessage)
                {
                    db.messages.Remove(item);
                }
                db.SaveChanges();
            }

            var currentUser = db.Users.Find(User.Identity.GetUserId());
            var model = new ReadMessageFromUserViewModel();
            var query = from b in db.messages where b.ReceiverUser.Id.Equals(currentUser.Id) where b.SenderUser.UserName.Equals(username)  select new { b.Headline, b.Date, b.IsRead, b.MessageId};
            model.message = new List<helpClass>();
            foreach(var item in query)
            {
                model.message.Add(new helpClass
                {
                    Header = item.Headline,
                    date = item.Date,
                    isRead = item.IsRead,
                    id = item.MessageId
                });
            }
            model.deletedMessages = currentUser.DeletedMessages;
            var totalMessages = 0;
            var totalReadMessages = 0;
            var query2 = from b in db.messages
                        where b.ReceiverUser.Id.Equals(currentUser.Id)
                        select new { b.MessageId, b.IsRead };

            foreach (var item in query2)
            {
                totalMessages++;
                if (item.IsRead)
                {
                    totalReadMessages++;
                }
            }
            model.totalMessages = totalMessages;
            model.totalReadMessages = totalReadMessages;
            return View(model);
        }

        public ActionResult ShowMessage(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int x = id.GetValueOrDefault();
            var db = new ApplicationDbContext();
            Message message = db.messages.Find(x);
            if (message == null)
            {
                return HttpNotFound();
            }
            var model = new ShowMessageViewModel();
            var readMessageQuery = (from b in db.messages where b.MessageId.Equals(x) select b).Single();
            readMessageQuery.IsRead = true;
            db.SaveChanges();
            
            var query = from b in db.messages where b.MessageId.Equals(x) select new { b.Headline, b.Text, b.SenderUser.UserName };
            foreach(var item in query)
            {
                model.Header = item.Headline;
                model.Text = item.Text;
                model.username = item.UserName;
                model.Id = x;
            }
            var currentUser = db.Users.Find(User.Identity.GetUserId());
            model.deletedMessages = currentUser.DeletedMessages;
            var totalMessages = 0;
            var totalReadMessages = 0;
            var query2 = from b in db.messages
                         where b.ReceiverUser.Id.Equals(currentUser.Id)
                         select new { b.MessageId, b.IsRead };

            foreach (var item in query2)
            {
                totalMessages++;
                if (item.IsRead)
                {
                    totalReadMessages++;
                }
            }
            model.totalMessages = totalMessages;
            model.totalReadMessages = totalReadMessages;


            return View(model);
        }
    }

   
}