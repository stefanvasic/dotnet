﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVCMessage.Models
{
    public class Message
    {
        [Key]
        public int MessageId { get; set; }
        public string Receiver { get; set; }
        public string Headline { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
        public Boolean IsRead { get; set; }
        public virtual ApplicationUser ReceiverUser { get; set; }
        public virtual ApplicationUser SenderUser { get; set; }

        //Navigation property

        //public virtual ApplicationUser User { get; set; }
    }

    public class UserDBContext : DbContext
    {
        public DbSet<Message> Messages { get; set; }
    }

    
}