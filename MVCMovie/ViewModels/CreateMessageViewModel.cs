﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MVCMessage.ViewModels
{
    public class CreateMessageViewModel
    {
       
        [Required(ErrorMessage ="Send to is required")]
        public string Receiver { get; set; }
        [Display(Name = "Send to")]
        public SelectList ReceiverList { get; set; }
        [Required(ErrorMessage = "Headline is required")]
        public string Headline { get; set; }
        [DataType(DataType.MultilineText)]
        [Display(Name ="Your message")]
        public string Text { get; set; }
        public string Message { get; set; }
    }
}