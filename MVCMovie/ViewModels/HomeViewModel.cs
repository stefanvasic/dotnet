﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MVCMessage.ViewModels
{
    public class HomeViewModel
    {
        public String UserName { get; set; }
        public int? NrOfLoggins { get; set; } = 1;
        [Display(Name = "Release Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime LastLoggedIn { get; set; } = DateTime.Now;
        public int UnReadMessages { get; set; } = 0;
       
    }
}