﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCMessage.ViewModels
{
    public class ReadMessageFromUserViewModel
    {
        public List<helpClass> message { get; set; }
        public int totalMessages { get; set; }
        public int totalReadMessages { get; set; }
        public int deletedMessages { get; set; }
    }
    public class helpClass
    {
        public String Header { get; set; }
        public DateTime date { get; set; }
        public Boolean isRead { get; set; }
        public int id { get; set; }
        
    }
}