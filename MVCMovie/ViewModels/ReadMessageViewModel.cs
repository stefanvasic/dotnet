﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCMessage.ViewModels
{
    public class ReadMessageViewModel
    {
        public List<String> usernames { get; set; }
        public int totalMessages { get; set; }
        public int totalReadMessages { get; set; }
        public int deletedMessages { get; set; }
    }
}