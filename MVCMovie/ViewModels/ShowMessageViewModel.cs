﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCMessage.ViewModels
{
    public class ShowMessageViewModel
    {
        public String Header { get; set; }
        public String Text { get; set; }
        public int Id { get; set; }
        public String username { get; set; }
        public int totalMessages { get; set; }
        public int totalReadMessages { get; set; }
        public int deletedMessages { get; set; }

    }
}